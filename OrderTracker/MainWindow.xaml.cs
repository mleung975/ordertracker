﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Coding4Fun.Kinect.Wpf;
using System.Threading;
using System.Timers;
using System.Windows.Threading;
using System.Collections;
using System.Media;
using System.Web;

namespace OrderTracker
{

    public enum OrderStatus { Normal, Hover, Claimed, Done };

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region global vars
        //Arrays have to be one size greater than the total amount of orders that can fit on the screen (13)
        DockPanel[] OrderBoxes = new DockPanel[15];
        Order[] OpenOrders = new Order[16];
        Order[] ClosedOrders = new Order[16];
        Border[] OrderBorders = new Border[15];
        int state = 1;
        int lastState = 1;
        bool fanopen = false;
        int numClosed = 0;
        int numOpened = 0;
        SortedList skeletonHist = new SortedList();
        SortedList rHist = new SortedList();
        SortedList lHist = new SortedList();
        bool tracking = false;
        bool delay = false;
        bool starting = true;
        //bool isStartScreen = true;

        //stuff from my (Matt's) IPA3 involving using the cursor.  I'll try to clean up the unnecessary stuff
        bool playing = true;
        Dispatcher _mainDispatcher = Dispatcher.CurrentDispatcher;
        System.Timers.Timer time = new System.Timers.Timer();
        System.Timers.Timer actionTime = new System.Timers.Timer();
        bool closing = false;
        const int skeletonCount = 6;
        Skeleton[] allSkeletons = new Skeleton[skeletonCount];

        String toDisplay = "";
        int lastIndex = 0;
        int menuIndex = 0;
        bool swipeMethod = true;

        double top = 0;
        double bottom = 0;
        double left = 0;
        double right = 0;
        double itemLeft = 0;
        double itemTop = 0;
        double itemRight = 0;
        double itemBottom = 0;
        int touching;

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            this.WindowState = WindowState.Maximized;
            PopulateOrderLocation();
        }

        #region Switching between Screens
        //test

        private void GoToClosed()
        {
            //transitions from state 1 to state 3
            ClosedLayout();
            for (int i = 0; i < numClosed; i++)
            {
                OrderBoxes[i].Visibility = System.Windows.Visibility.Visible;
                OrderBorders[i].Visibility = System.Windows.Visibility.Visible;
                AddContent(i, false);
            }
            for (int i = numClosed; i < numOpened; i++)
            {
                OrderBoxes[i].Visibility = System.Windows.Visibility.Collapsed;
                OrderBorders[i].Visibility = System.Windows.Visibility.Collapsed;
            }
            state = 3;
        }

        private void GoToHelp()
        {
            //transitions from any screen to the help screen
            //changes state to state 5, the help screen state and stores the current state in the lastState global variable so can return
            if (state == 2)
            {
                ClosePopUp();
            }
            if (state == 4)
            {
                CloseClosed();
            }
            if (state == 1)
            {
                for (int i = 0; i < numOpened; i++)
                {
                    OrderBorders[i].Visibility = System.Windows.Visibility.Collapsed;
                    OrderBoxes[i].Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            if (state == 3)
            {
                for (int i = 0; i < numClosed; i++)
                {
                    OrderBorders[i].Visibility = System.Windows.Visibility.Collapsed;
                    OrderBoxes[i].Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            MainScreens.Visibility = System.Windows.Visibility.Collapsed;
            HelpScreen.Visibility = System.Windows.Visibility.Visible;
            foreach (UIElement child in HelpScreen.Children)
            {
                child.Visibility = System.Windows.Visibility.Visible;
            }
            OrderWindow.Visibility = System.Windows.Visibility.Collapsed;
            CompletedOrders.Visibility = System.Windows.Visibility.Collapsed;
            lastState = state;
            state = 5;
        }

        private void CloseHelp()
        {
            HelpScreen.Visibility = System.Windows.Visibility.Collapsed;
            MainScreens.Visibility = System.Windows.Visibility.Visible;
            if (lastState == 1)
            {
                GoToOpened();
                state = 1;
            }
            if (lastState == 2)
            {
                BigOrder(touching - 1, 1);
                state = 2;
            }
            if (lastState == 3)
            {
                GoToClosed();
                state = 3;
            }
            if (lastState == 4)
            {
                BigOrder(touching - 1, 3);
                state = 4;
            }
            OrderWindow.Visibility = System.Windows.Visibility.Visible;
            CompletedOrders.Visibility = System.Windows.Visibility.Visible;
        }

        private void ClosedLayout()
        {
            Canvas.SetLeft(CompletedOrders, 8);
            Canvas.SetLeft(OrderWindow, 342);
            CompletedOrders.Header = "Open";
            OrderWindow.Header = "Completed Orders";
            listBox.Items.Clear();
            for (int i = 0; i < numOpened; i++)
            {
                listBox.Items.Add("Order " + OpenOrders[i].getOrderNum());
            }
        }

        private void OpenLayout()
        {
            Canvas.SetLeft(OrderWindow, 8);
            Canvas.SetLeft(CompletedOrders, 1042);
            OrderWindow.Header = "Open Orders";
            CompletedOrders.Header = "Completed";
            listBox.Items.Clear();
            for (int i = 0; i < numClosed; i++)
            {
                listBox.Items.Add("Order " + ClosedOrders[i].getOrderNum());
            }
        }

        #endregion

        #region Selecting order

        private void BigOrder(int OrderBoxNum, int state)
        {
            //fills in the order details in the pop up box
            //makes the right buttons visible depending on if transitioned from state 1 or 3
            currentOrder.Visibility = System.Windows.Visibility.Visible;            
            if (state == 1 && tracking)
            {
                currentOrderName.Text = "Order No. " + OpenOrders[OrderBoxNum].getOrderNum();
                currentOrderDetails.Text = OpenOrders[OrderBoxNum].getOrderDetails();
                currentOrderPlacedAt.Text = "Placed at: " + OpenOrders[OrderBoxNum].getHour() + ":" + OpenOrders[OrderBoxNum].getMinute() + OpenOrders[OrderBoxNum].getAMorPM();
                claimOrder.Visibility = System.Windows.Visibility.Visible;
                markDone.Visibility = System.Windows.Visibility.Visible;
            }
            if (state == 3 && tracking)
            {
                currentOrderName.Text = "Order No. " + ClosedOrders[OrderBoxNum].getOrderNum();
                currentOrderDetails.Text = ClosedOrders[OrderBoxNum].getOrderDetails();
                currentOrderPlacedAt.Text = "Placed at: " + ClosedOrders[OrderBoxNum].getHour() + ":" + ClosedOrders[OrderBoxNum].getMinute() + ClosedOrders[OrderBoxNum].getAMorPM();
                reopenOrder.Visibility = System.Windows.Visibility.Visible;
            }
        }

        #endregion

        #region State 1 Methods (Main Screen)

        private void SelectOrder(int OrderNum)
        {
            //brings you to state 2, with all of the details for the order visible in the pop up, instantiate correct buttons
            for (int i = 0; i < numOpened; i++)
            {
                OrderBoxes[i].Visibility = System.Windows.Visibility.Collapsed;
                OrderBorders[i].Visibility = System.Windows.Visibility.Collapsed;
            }
            BigOrder(OrderNum, state);
            state = 2;
        }
        #endregion

        #region State 2 Methods (Order Pop up)

        private void ClaimOrder(int OrderNum)
        {
            //Mark orders as claimed, highlight their background and return to state 1
            for (int i = 0; i < numOpened; i++)
            {
                OrderBoxes[i].Visibility = System.Windows.Visibility.Visible;
            }
            //change the border of the OrderNum
            ClosePopUp();
            OpenOrders[OrderNum].setStatus(OrderStatus.Claimed);
            SetOrderStyle(OrderStatus.Claimed, OrderBorders[OrderNum]);
        }

        private void MarkDone(int OrderNum)
        {
            //Makes order invisible, order number becomes visible in the completed order screen, moves over all other orders to get rid of empty space, adds order to completed orders column
            for (int i = 0; i < numOpened; i++)
            {
                OrderBoxes[i].Visibility = System.Windows.Visibility.Visible;
                OrderBorders[i].Visibility = System.Windows.Visibility.Visible;
            }
            OpenOrders[OrderNum].setStatus(OrderStatus.Normal);
            SetOrderStyle(OrderStatus.Normal, OrderBorders[OrderNum]);
            ClosePopUp();
            UpdateOrders(OrderNum, true);

            String fanNum = "fan" + (OrderNum + 1);
            Image fanOrder = (Image)this.FindName(fanNum);
            fanOrder.Visibility = System.Windows.Visibility.Collapsed;

            //String fanbox = "fanorder" + (OrderNum + 1);
            //TextBlock fanName = (TextBlock)this.FindName(fanbox);
            //fanName.Visibility = System.Windows.Visibility.Collapsed;

            state = 1;
        }

        private void ClosePopUp()
        {
            currentOrder.Visibility = System.Windows.Visibility.Collapsed;
            markDone.Visibility = System.Windows.Visibility.Collapsed;
            claimOrder.Visibility = System.Windows.Visibility.Collapsed;

            //returns you to state 1
            for (int i = 0; i < numOpened; i++)
            {
                OrderBoxes[i].Visibility = System.Windows.Visibility.Visible;
                OrderBorders[i].Visibility = System.Windows.Visibility.Visible;
            }
            state = 1;
        }

        #endregion

        #region State 3 Methods (Closed Order Screen)

        private void SelectClosed(int OrderNum)
        {
            //brings you to state 4, with all the details for the order visible in the pop up, instantiate the correct buttons
            for (int i = 0; i < numClosed; i++)
            {
                OrderBoxes[i].Visibility = System.Windows.Visibility.Collapsed;
                OrderBorders[i].Visibility = System.Windows.Visibility.Collapsed;
            }
            BigOrder(OrderNum, state);
            state = 4;
        }

        private void GoToOpened()
        {
            //transition from state 3 to 1
            if (state == 3 || state == 5)
            {
                OpenLayout();
                for (int i = 0; i < numOpened; i++)
                {
                    OrderBoxes[i].Visibility = System.Windows.Visibility.Visible;
                    OrderBorders[i].Visibility = System.Windows.Visibility.Visible;
                    AddContent(i, true);
                }
                for (int i = numOpened; i < numClosed; i++)
                {
                    OrderBoxes[i].Visibility = System.Windows.Visibility.Collapsed;
                    OrderBorders[i].Visibility = System.Windows.Visibility.Collapsed;
                }
                state = 1;
            }
            /*
            if (state == 0)
            {
                OpenLayout();
                MAINSCREENTITLE.Visibility = System.Windows.Visibility.Collapsed;
                MAINSCREENHELP.Visibility = System.Windows.Visibility.Collapsed;
                MAINSCREENSTART.Visibility = System.Windows.Visibility.Collapsed;
                for (int i = numOpened; i < numClosed; i++)
                {
                    OrderBoxes[i].Visibility = System.Windows.Visibility.Collapsed;
                    OrderBorders[i].Visibility = System.Windows.Visibility.Collapsed;
                }
            }
             * */
        }

        #endregion

        #region State 4 Methods (Pop up in the closed order screen)

        private void ReOpen(int OrderNum)
        {
            //Make the last dockpanel visible, make the details the details fo the reopened order, adds order to the open orders column
            for (int i = 0; i < numClosed; i++)
            {
                OrderBoxes[i].Visibility = System.Windows.Visibility.Visible;
                OrderBorders[i].Visibility = System.Windows.Visibility.Visible;
            }
            CloseClosed();
            UpdateOrders(OrderNum, false);

            String fanNum = "otherfan" + (OrderNum + 1);
            Image fanOrder = (Image)this.FindName(fanNum);
            fanOrder.Visibility = System.Windows.Visibility.Collapsed;

            //String fanbox = "fanorder" + (OrderNum + 1);
            //TextBlock fanName = (TextBlock)this.FindName(fanbox);
            //fanName.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void CloseClosed()
        {
            currentOrder.Visibility = System.Windows.Visibility.Collapsed;
            reopenOrder.Visibility = System.Windows.Visibility.Collapsed;

            //returns you to state 3
            for (int i = 0; i < numClosed; i++)
            {
                OrderBoxes[i].Visibility = System.Windows.Visibility.Visible;
                OrderBorders[i].Visibility = System.Windows.Visibility.Visible;
            }
            state = 3;
        }

        #endregion

        #region Building content/populating content methods

        //move all of the other order information to the dockpanel of the removed order, make the last dockpanel invisible
        private void UpdateOrders(int RemovedNum, bool closing)
        {
            if (closing)
            {
                listBox.Items.Add("Order " + OpenOrders[RemovedNum].getOrderNum());
                ClosedOrders[numClosed] = OpenOrders[RemovedNum];
                numClosed++;
                for (int i = RemovedNum; i < numOpened - 1; i++)
                {
                    OpenOrders[i] = OpenOrders[i + 1];
                    AddContent(i, closing);
                }
                OrderBoxes[numOpened - 1].Visibility = System.Windows.Visibility.Collapsed;
                OrderBorders[numOpened - 1].Visibility = System.Windows.Visibility.Collapsed;
                numOpened--;
            }
            else
            {
                listBox.Items.Add("Order " + ClosedOrders[RemovedNum].getOrderNum());
                OpenOrders[numOpened] = ClosedOrders[RemovedNum];
                numOpened++;
                for (int i = RemovedNum; i < numClosed - 1; i++)
                {
                    ClosedOrders[i] = ClosedOrders[i + 1];
                    AddContent(i, closing);
                }
                OrderBoxes[numClosed - 1].Visibility = System.Windows.Visibility.Collapsed;
                OrderBorders[numClosed - 1].Visibility = System.Windows.Visibility.Collapsed;
                numClosed--;
            }
        }

        private void AddContent(int BoxNum, bool closing)
        {
            //make all of the order details and numbers visible in the corresponding dockpanels
            //if closing is true you are in the open orders screen, false is the opposite
            if (closing)
            {
                //get the info to be displayed in the order box
                int id = OpenOrders[BoxNum].getOrderNum();
                String details = OpenOrders[BoxNum].getOrderDetails();
                int hour = OpenOrders[BoxNum].getHour();
                int minute = OpenOrders[BoxNum].getMinute();
                String AMorPM = OpenOrders[BoxNum].getAMorPM();

                /*
                DockPanel stackPanel = OrderBoxes[BoxNum];
                UIElementCollection children = stackPanel.Children;
                foreach (UIElement child in children)
                {
                    Label control = (Label)child;
                    control.Content = 
                }*/

                //build up the unique names for the labels in the order box
                BoxNum++;
                String orderName = "order" + BoxNum + "name";
                String orderDetails = "order" + BoxNum + "details";
                String orderPlacedAt = "order" + BoxNum + "placedat";

                //grab the labels by their names
                TextBlock nameLabel = (TextBlock)this.FindName(orderName);
                TextBlock detailsLabel = (TextBlock)this.FindName(orderDetails);
                TextBlock placedAtLabel = (TextBlock)this.FindName(orderPlacedAt);

                //set the contents of the labels to the correct info
                nameLabel.Text = "Order No. " + id;
                detailsLabel.Text = details;
                placedAtLabel.Text = "Placed: " + hour + ":" + minute + AMorPM;
            }
            else
            {
                //get the info to be displayed in the order box
                int id = ClosedOrders[BoxNum].getOrderNum();
                String details = ClosedOrders[BoxNum].getOrderDetails();
                int hour = ClosedOrders[BoxNum].getHour();
                int minute = ClosedOrders[BoxNum].getMinute();
                String AMorPM = ClosedOrders[BoxNum].getAMorPM();

                //build up the unique names for the labels in the order box
                BoxNum++;
                String orderName = "order" + BoxNum + "name";
                String orderDetails = "order" + BoxNum + "details";
                String orderPlacedAt = "order" + BoxNum + "placedat";

                //grab the labels by their names
                TextBlock nameLabel = (TextBlock)this.FindName(orderName);
                TextBlock detailsLabel = (TextBlock)this.FindName(orderDetails);
                TextBlock placedAtLabel = (TextBlock)this.FindName(orderPlacedAt);

                //set the contents of the labels to the correct info
                nameLabel.Text = "Order No. " + id;
                detailsLabel.Text = details;
                placedAtLabel.Text = "Placed: " + hour + ":" + minute + AMorPM;
            }
        }

        private void PopulateOrderLocation()
        {
            //instantiation ONLY.  Puts all of the orders in the Order Arrays
            OrderBoxes[0] = order1contents;
            OrderBoxes[1] = order2contents;
            OrderBoxes[2] = order3contents;
            OrderBoxes[3] = order4contents;
            OrderBoxes[4] = order5contents;
            OrderBoxes[5] = order6contents;
            /*
            OrderBoxes[6] = order7contents;
            OrderBoxes[7] = order8contents;
            OrderBoxes[8] = order9contents;
            OrderBoxes[9] = order10contents;
            OrderBoxes[10] = order11contents;
            OrderBoxes[11] = order12contents;
            */
            OpenOrders[0] = new Order(67, "Table #4\n1 Caesar Salad\n1 Pepsi", 7, 44, "pm");
            OpenOrders[1] = new Order(66, "Table #13\n1 Hamburger\n1 Lemonade\n1 Banana", 7, 40, "pm");
            OpenOrders[2] = new Order(65, "Table #13\n1 Cheeseburger\n1 French Fries", 7, 32, "pm");
            /*
            OpenOrders[3] = new Order(1, "4 drink \n 4 hamburger", 1, 30, "p.m.");
            OpenOrders[4] = new Order(1, "5 drink \n 5 hamburger", 1, 30, "p.m.");
            OpenOrders[5] = new Order(1, "6 drink \n 6 hamburger", 1, 30, "p.m.");
            OpenOrders[6] = new Order(1, "7 drink \n 7 hamburger", 1, 30, "p.m.");
            OpenOrders[7] = new Order(1, "8 drink \n 8 hamburger", 1, 30, "p.m.");
            OpenOrders[8] = new Order(1, "9 drink \n 9 hamburger", 1, 30, "p.m.");
            OpenOrders[9] = new Order(1, "10 drink \n 10 hamburger", 1, 30, "p.m.");
            OpenOrders[10] = new Order(1, "11 drink \n 11 hamburger", 1, 30, "p.m.");
            OpenOrders[11] = new Order(1, "12 drink \n 12 hamburger", 1, 30, "p.m.");
            */
            ClosedOrders[0] = new Order(1, "1 drink \n1 hamburger", 1, 30, "pm");
            ClosedOrders[1] = new Order(2, "2 drink \n2 hamburger", 1, 30, "pm");
            ClosedOrders[2] = new Order(3, "3 drink \n3 hamburger", 1, 30, "pm");
            /*
            ClosedOrders[3] = new Order(1, "4 drink \n 4 hamburger", 1, 30, "p.m.");
            ClosedOrders[4] = new Order(1, "5 drink \n 5 hamburger", 1, 30, "p.m.");
            ClosedOrders[5] = new Order(1, "6 drink \n 6 hamburger", 1, 30, "p.m.");
            ClosedOrders[6] = new Order(1, "7 drink \n 7 hamburger", 1, 30, "p.m.");
            ClosedOrders[7] = new Order(1, "8 drink \n 8 hamburger", 1, 30, "p.m.");
            ClosedOrders[8] = new Order(1, "9 drink \n 9 hamburger", 1, 30, "p.m.");
            ClosedOrders[9] = new Order(1, "10 drink \n 10 hamburger", 1, 30, "p.m.");
            ClosedOrders[10] = new Order(1, "11 drink \n 11 hamburger", 1, 30, "p.m.");
            ClosedOrders[11] = new Order(1, "12 drink \n 12 hamburger", 1, 30, "p.m.");
            */
            OrderBorders[0] = order1;
            OrderBorders[1] = order2;
            OrderBorders[2] = order3;
            OrderBorders[3] = order4;
            OrderBorders[4] = order5;
            OrderBorders[5] = order6;
            /*
            OrderBorders[6] = order7;
            OrderBorders[7] = order8;
            OrderBorders[8] = order9;
            OrderBorders[9] = order10;
            OrderBorders[10] = order11;
            OrderBorders[11] = order12;
            */
            numOpened = 3;
            numClosed = 3;
        }

        #endregion

        #region Changing XML Styles

        public void SetOrderStyle(OrderStatus status, object sender)
        {
            if (status == OrderStatus.Normal)
                ((Border)sender).Style = (Style)FindResource("OrderNormal");
            else if (status == OrderStatus.Claimed)
                ((Border)sender).Style = (Style)FindResource("OrderClaimed");
            else if (status == OrderStatus.Hover)
                ((Border)sender).Style = (Style)FindResource("OrderHover");
            //else if (status == OrderStatus.Done)
            //delete order from this view or set it invisible or something
        }

        public void SetButtonStyle(OrderStatus status, object sender)
        {
            if (status == OrderStatus.Normal)
                ((Button)sender).Style = (Style)FindResource("ButtonNormal");
            else if (status == OrderStatus.Hover)
                ((Button)sender).Style = (Style)FindResource("ButtonHover");
        }

        public void HoverOver()
        {
            //Makes the other version of the order visible
            int n = OrderTouching();
            if (state == 1)
            {
                String fanName = "fan" + n;
                Image fan = (Image)this.FindName(fanName);
                fan.Visibility = System.Windows.Visibility.Visible;
            }
            else if (state == 3)
            {
                String fanName = "otherfan" + n;
                Image fan = (Image)this.FindName(fanName);
                fan.Visibility = System.Windows.Visibility.Visible;
            }

            //String orderNum = "fanorder" + n;
            //TextBlock order = (TextBlock)this.FindName(orderNum);
            //order.Visibility = System.Windows.Visibility.Visible;

            //sets touching to that order num
            touching = n;

            //changes state to 6 which is the state for gestures with the left hand to mark or close orders.
            fanopen = true;
        }

        #endregion

        #region DispatcherTimer methods

        void actionTime_Elapsed(object sender, ElapsedEventArgs e)
        {
            _mainDispatcher.BeginInvoke(DispatcherPriority.Normal, new ThreadStart(delegate()
            {
                GoToClosed();
                GoToOpened();
            }));
        }

        void time_Elapsed(object sender, ElapsedEventArgs e)
        {
            _mainDispatcher.BeginInvoke(DispatcherPriority.Normal, new ThreadStart(delegate()
            {

            }));
        }

        private void markTick(object sender, EventArgs e)
        {
            ((DispatcherTimer)sender).Stop();
            MarkDone(touching - 1);
            playing = true;
        }

        private void selectTick(object sender, EventArgs e)
        {
            ((DispatcherTimer)sender).Stop();
            playing = true;
        }

        private void select()
        {
            if (OrderTouching() == 19)
            {
                GoToHelp();
            }
            else if (state == 0)
            {
                if (OrderTouching() == 20)
                {
                    GoToOpened();
                }
            }
            else if (state == 1)
            {
                if (OrderTouching() != 0)
                {
                    SelectOrder(OrderTouching() - 1);
                }
            }
            else if (state == 2)
            {
                if (OrderTouching() == 16)
                {
                    ClaimOrder(touching - 1);
                }
                else if (OrderTouching() == 17)
                {
                    MarkDone(touching - 1);
                }
            }

            else if (state == 3)
            {
                if (OrderTouching() != 0)
                {
                    SelectClosed(OrderTouching() - 1);
                }
            }

            else if (state == 4)
            {
                if (OrderTouching() == 18)
                {
                    ReOpen(touching - 1);
                }
            }
        }

        #endregion

        private void Tracking(object sender, EventArgs e)
        {
            ((DispatcherTimer)sender).Stop();
            if (tracking)
            {
                tracking = false;
                playing = true;
            }
            else
            {
                tracking = true;
                playing = true;
            }
            delay = true;
        }

        void newSensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            if (closing)
            {
                return;
            }

            //Get a skeleton
            Skeleton first = GetFirstSkeleton(e);

            if (first == null)
            {
                return;
            }

            if (first.TrackingState == SkeletonTrackingState.NotTracked)
            {
                return;
            }

            if (first.Joints[JointType.HandLeft].Position.X > first.Joints[JointType.HandRight].Position.X
                && first.Joints[JointType.HandLeft].Position.Y > first.Joints[JointType.ElbowLeft].Position.Y
                && first.Joints[JointType.HandRight].Position.Y > first.Joints[JointType.ElbowRight].Position.Y
                && first.Joints[JointType.HandLeft].Position.Y < first.Joints[JointType.Head].Position.Y
                && first.Joints[JointType.HandRight].Position.Y < first.Joints[JointType.Head].Position.Y
                && (first.Joints[JointType.HandLeft].Position.Z - first.Joints[JointType.HandRight].Position.Z < 0.001
                     || first.Joints[JointType.HandLeft].Position.Z - first.Joints[JointType.HandRight].Position.Z < 0.001)
                && !delay)
            {
                if (starting)
                {
                    StartScreen.Visibility = System.Windows.Visibility.Collapsed;
                    MainScreens.Visibility = System.Windows.Visibility.Visible;
                    starting = false;
                    tracking = true;
                }
                else
                {
                    if (tracking)
                    {
                        tracking = false;
                        playing = true;
                    }
                    else
                    {
                        tracking = true;
                        playing = true;
                    }
                }
                delay = true;
                /*
                DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
                timer.Tick += new EventHandler(Tracking);
                timer.Interval = TimeSpan.FromSeconds(1);
                playing = false;
                timer.Start();
                 */
            }

            if (first.Joints[JointType.HandLeft].Position.X < first.Joints[JointType.HandRight].Position.X)
            {
                delay = false;
            }
            //TESTING STUFFS
            /*
            if (playing == false)
            {
                canvas.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                canvas.Visibility = System.Windows.Visibility.Visible;
            }
            */
            //PROGRAM CROSSING HANDS HERE TO ACTIVATE TRACKING
            //check that x's of right and left hand cross over
            //run dispatcher timer for a second
            if (tracking && !delay)
            {
                /*
                if (isStartScreen)
                {
                    StartScreen.Visibility = System.Windows.Visibility.Collapsed;
                    MainScreens.Visibility = System.Windows.Visibility.Visible;
                }
                else*/
                //{
                    MainScreens.Opacity = 1.0;
                    IdleScreen.Visibility = System.Windows.Visibility.Collapsed;
                //}
                rightEllipse.Visibility = System.Windows.Visibility.Visible;
                ScalePosition(rightEllipse, first.Joints[JointType.HandRight]);
                GetCameraPoint(first, e);

                if (skeletonHist.Count >= 20 && rHist.Count >= 20 && lHist.Count >= 20)
                {
                    Array right1 = ((PointCollection)rHist.GetByIndex(lastIndex - 20)).ToArray();
                    Array right2 = ((PointCollection)rHist.GetByIndex(lastIndex - 10)).ToArray();
                    Array right3 = ((PointCollection)rHist.GetByIndex(lastIndex - 1)).ToArray();
                    double XR1 = ((Point)right1.GetValue(0)).X;
                    double YR1 = ((Point)right1.GetValue(1)).Y;
                    double ZR1 = ((Point)right1.GetValue(0)).Y;
                    double XR2 = ((Point)right2.GetValue(0)).X;
                    double YR2 = ((Point)right2.GetValue(1)).Y;
                    double ZR2 = ((Point)right2.GetValue(0)).Y;
                    double XR3 = ((Point)right3.GetValue(0)).X;
                    double YR3 = ((Point)right3.GetValue(1)).Y;
                    double ZR3 = ((Point)right3.GetValue(0)).Y;

                    Array left1 = ((PointCollection)lHist.GetByIndex(lastIndex - 20)).ToArray();
                    Array left2 = ((PointCollection)lHist.GetByIndex(lastIndex - 10)).ToArray();
                    Array left3 = ((PointCollection)lHist.GetByIndex(lastIndex - 1)).ToArray();
                    double XL1 = ((Point)left1.GetValue(0)).X;
                    double YL1 = ((Point)left1.GetValue(1)).Y;
                    double ZL1 = ((Point)left1.GetValue(0)).Y;
                    double XL2 = ((Point)left2.GetValue(0)).X;
                    double YL2 = ((Point)left2.GetValue(1)).Y;
                    double ZL2 = ((Point)left2.GetValue(0)).Y;
                    double XL3 = ((Point)left3.GetValue(0)).X;
                    double YL3 = ((Point)left3.GetValue(1)).Y;
                    double ZL3 = ((Point)left3.GetValue(0)).Y;

                    Array body1 = ((PointCollection)skeletonHist.GetByIndex(lastIndex - 20)).ToArray();
                    Array body2 = ((PointCollection)skeletonHist.GetByIndex(lastIndex - 10)).ToArray();
                    Array body3 = ((PointCollection)skeletonHist.GetByIndex(lastIndex - 1)).ToArray();
                    double X1 = ((Point)body1.GetValue(0)).X;
                    double Y1 = ((Point)body1.GetValue(1)).Y;
                    double Z1 = ((Point)body1.GetValue(0)).Y;
                    double X2 = ((Point)body2.GetValue(0)).X;
                    double Y2 = ((Point)body2.GetValue(1)).Y;
                    double Z2 = ((Point)body2.GetValue(0)).Y;
                    double X3 = ((Point)body3.GetValue(0)).X;
                    double Y3 = ((Point)body3.GetValue(1)).Y;
                    double Z3 = ((Point)body3.GetValue(0)).Y;
                    bool bodyStay = X3 > (X1 - .1) || X3 < (X1 + .1) && Z3 > (Z1 - .1) || Z3 < (Z1 + .1);
                    bool rVertRange = YR3 > (YR1 - .00001) || (YR3 < YR1 + .00001);
                    if (first.Joints[JointType.HandLeft].Position.Y > first.Joints[JointType.Head].Position.Y)
                    {
                        bool swipeLeft = XR1 * 100.0 > (XR2 + .2) * 100.0 && XR2 * 100.0 > (XR3 + .2) * 100.0 && bodyStay && rVertRange;
                        if (swipeLeft && state == 1)
                        {

                            if (playing)
                            {
                                playing = false;
                                DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
                                timer.Tick += new EventHandler(TransitionToClosed);
                                timer.Interval = TimeSpan.FromSeconds(2);
                                GoToClosed();
                                timer.Start();
                            }
                            time.Enabled = false;
                        }
                        else
                        {
                            bool swipeRight = (XR1 + .2) * 100.0 < XR2 * 100.0 && (XR2 + .2) * 100.0 < XR3 * 100.0 && XR1 != 0 && XR2 != 0 && XR3 != 0 && bodyStay && rVertRange;
                            if (swipeRight && state == 3)
                            {
                                if (playing)
                                {
                                    playing = false;
                                    DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
                                    timer.Tick += new EventHandler(TransitionToOpened);
                                    timer.Interval = TimeSpan.FromSeconds(2);
                                    GoToOpened();
                                    timer.Start();
                                }
                                time.Enabled = false;
                            }
                        }
                    }

                    if (fanopen)
                    {
                        //bool notMoving = XR1 < XR2 + 0.001 && XR1 > XR2 + 0.001;
                        bool swipeLeft = XL1 > (XL2 + .2) && bodyStay;
                        bool wiper = first.Joints[JointType.HandLeft].Position.Y > first.Joints[JointType.ElbowLeft].Position.Y;
                        bool active = wiper && first.Joints[JointType.HandLeft].Position.Z < first.Joints[JointType.HandRight].Position.Z;
                        bool swipeRight = (XL1 + .2) < XL2 && XL1 != 0 && XL2 != 0 && XL3 != 0 && bodyStay;

                        if (wiper)
                        {
                            if (playing)
                            {
                                if (state == 1)
                                {
                                    if (swipeLeft)
                                    {
                                        playing = false;
                                        DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
                                        timer.Tick += new EventHandler(selectTick);
                                        timer.Interval = TimeSpan.FromSeconds(1);
                                        timer.Start();
                                        if (OrderTouching() != 0)
                                        {
                                            ClaimOrder(OrderTouching() - 1);
                                        }
                                    }
                                    else if (swipeRight)
                                    {
                                        playing = false;
                                        DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
                                        timer.Tick += new EventHandler(selectTick);
                                        timer.Interval = TimeSpan.FromSeconds(1);
                                        timer.Start();
                                        if (OrderTouching() != 0)
                                        {
                                            MarkDone(OrderTouching() - 1);
                                        }
                                    }
                                }
                                else if (state == 3)
                                {
                                    if (swipeLeft)
                                    {
                                        playing = false;
                                        DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
                                        timer.Tick += new EventHandler(selectTick);
                                        timer.Interval = TimeSpan.FromSeconds(1);
                                        timer.Start();
                                        if (OrderTouching() != 0)
                                        {
                                            ReOpen(OrderTouching() - 1);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Detects whether or not you are hovering over visible orders
                    if (OrderTouching() != 0)
                    {
                        if (OrderTouching() < 15)
                        {
                            //if (state == 1)
                            //{
                            HoverOver();
                            //}
                            /*else
                            {
                                SetOrderStyle(OrderStatus.Hover, OrderBorders[OrderTouching() - 1]);
                            }*/
                            touching = OrderTouching();
                        }
                        else
                        {
                            Button b;
                            //Button b2;
                            if (OrderTouching() == 16)
                            {
                                b = claimOrder;
                            }
                            else if (OrderTouching() == 17)
                            {
                                b = markDone;
                            }
                            else if (OrderTouching() == 18)
                            {
                                b = reopenOrder;
                            }
                            else //if (OrderTouching() == 19)
                            {
                                b = help;
                                //b2 = MAINSCREENHELPBUTTON;
                            }
                            /*
                        else
                        {
                            b = StartButton;
                        }
                             * */
                            if (OrderTouching() == 19)
                            {
                                SetButtonStyle(OrderStatus.Hover, b);
                                //SetButtonStyle(OrderStatus.Hover, b2);
                            }
                            else
                            {
                                SetButtonStyle(OrderStatus.Hover, b);
                            }
                        }
                    }
                    else
                    {
                        int length = 0;
                        if (state == 1)
                        {
                            length = numOpened;
                        }
                        else if (state == 3)
                        {
                            length = numClosed;
                        }
                        for (int i = 0; i < length; i++)
                        {
                            if (OrderBoxes[i].Visibility == System.Windows.Visibility.Visible)
                            {
                                if (state == 1 && OpenOrders[i].getStatus() == OrderStatus.Normal)
                                {
                                    SetOrderStyle(OrderStatus.Normal, OrderBorders[i]);
                                }
                                if (state == 1 && OpenOrders[i].getStatus() == OrderStatus.Claimed)
                                {
                                    SetOrderStyle(OrderStatus.Claimed, OrderBorders[i]);
                                }
                                if (state == 3 && ClosedOrders[i].getStatus() == OrderStatus.Normal)
                                {
                                    SetOrderStyle(OrderStatus.Normal, OrderBorders[i]);
                                }
                            }
                            if (state == 1)
                            {
                                String fanName = "fan" + (i + 1);
                                Image fan = (Image)this.FindName(fanName);
                                fan.Visibility = System.Windows.Visibility.Collapsed;
                            }
                            else if (state == 3)
                            {
                                String fanName = "otherfan" + (i + 1);
                                Image fan = (Image)this.FindName(fanName);
                                fan.Visibility = System.Windows.Visibility.Collapsed;
                            }
                            //String orderNum = "fanorder" + (i+1);
                            //TextBlock order = (TextBlock)this.FindName(orderNum);
                            //order.Visibility = System.Windows.Visibility.Collapsed;
                        }
                        if (state == 2)
                        {
                            SetButtonStyle(OrderStatus.Normal, claimOrder);
                            SetButtonStyle(OrderStatus.Normal, markDone);

                            String fanName = "fan" + touching;
                            Image fan = (Image)this.FindName(fanName);
                            fan.Visibility = System.Windows.Visibility.Collapsed;

                            //String orderNum = "fanorder" + touching;
                            //TextBlock order = (TextBlock)this.FindName(orderNum);
                            //order.Visibility = System.Windows.Visibility.Collapsed;
                        }
                        if (state == 4)
                        {
                            SetButtonStyle(OrderStatus.Normal, reopenOrder);

                            String fanName = "otherfan" + touching;
                            Image fan = (Image)this.FindName(fanName);
                            fan.Visibility = System.Windows.Visibility.Collapsed;

                            //String orderNum = "fanorder" + touching;
                            //TextBlock order = (TextBlock)this.FindName(orderNum);
                            //order.Visibility = System.Windows.Visibility.Collapsed;
                        }
                        if (state == 0)
                        {
                            //SetButtonStyle(OrderStatus.Normal, STARTBUTTON);
                            //SetButtonStyle(OrderStatus.Normal, MAINHELPBUTTON);
                        }
                        SetButtonStyle(OrderStatus.Normal, help);
                        fanopen = false;
                    }

                    //Code for making the selection
                    bool rightPush = ZR1 > ZR3 + 0.1 && bodyStay && rVertRange;
                    if (rightPush)
                    {
                        if (playing)
                        {
                            DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
                            timer.Tick += new EventHandler(selectTick);
                            timer.Interval = TimeSpan.FromSeconds(1);
                            playing = false;
                            timer.Start();
                            select();
                        }
                        time.Enabled = false;
                    }
                    else
                    {
                        //playing = true;
                        time.Enabled = false;
                        time.Interval = 1000;
                    }
                }
            }

            //Recross hands and make the kinect stop tracking your movement
            if (!tracking && !delay)
            {
                if (state == 2 || state == 4 || state == 5)
                {
                    DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
                    timer.Tick += new EventHandler(closeWindow);
                    timer.Interval = TimeSpan.FromSeconds(1);
                    playing = false;
                    timer.Start();
                    tracking = true;
                }
                else
                {
                    if (starting)
                    {
                        //do nothing
                    }
                    else
                    {
                        MainScreens.Opacity = 0.5;
                        IdleScreen.Visibility = System.Windows.Visibility.Visible;
                        rightEllipse.Visibility = System.Windows.Visibility.Collapsed;
                        foreach (UIElement fan in canvas.Children)
                        {
                            if (fan is Image)
                            {
                                fan.Visibility = System.Windows.Visibility.Collapsed;
                            }
                        }
                    }
                }
            }
        }

        #region Finding what cursor is over
        private int OrderTouching()
        {
            if (TouchingButton(order1contents, rightEllipse))
            {
                if (OrderBoxes[0].Visibility == System.Windows.Visibility.Visible)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            if (TouchingButton(order2contents, rightEllipse))
            {
                if (OrderBoxes[1].Visibility == System.Windows.Visibility.Visible)
                {
                    return 2;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(order3contents, rightEllipse))
            {
                if (OrderBoxes[2].Visibility == System.Windows.Visibility.Visible)
                {
                    return 3;
                }
                else
                {
                    return 0;
                }
            }
            if (TouchingButton(order4contents, rightEllipse))
            {
                if (OrderBoxes[3].Visibility == System.Windows.Visibility.Visible)
                {
                    return 4;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(order5contents, rightEllipse))
            {
                if (OrderBoxes[4].Visibility == System.Windows.Visibility.Visible)
                {
                    return 5;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(order6contents, rightEllipse))
            {
                if (OrderBoxes[5].Visibility == System.Windows.Visibility.Visible)
                {
                    return 6;
                }
                else
                {
                    return 0;
                }
            }
            /*
            if (TouchingButton(order7contents, rightEllipse))
            {
                if (OrderBoxes[6].Visibility == System.Windows.Visibility.Visible)
                {
                    return 7;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(order8contents, rightEllipse))
            {
                if (OrderBoxes[7].Visibility == System.Windows.Visibility.Visible)
                {
                    return 8;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(order9contents, rightEllipse))
            {
                if (OrderBoxes[8].Visibility == System.Windows.Visibility.Visible)
                {
                    return 9;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(order10contents, rightEllipse))
            {
                if (OrderBoxes[9].Visibility == System.Windows.Visibility.Visible)
                {
                    return 10;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(order11contents, rightEllipse))
            {
                if (OrderBoxes[10].Visibility == System.Windows.Visibility.Visible)
                {
                    return 11;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(order12contents, rightEllipse))
            {
                if (OrderBoxes[11].Visibility == System.Windows.Visibility.Visible)
                {
                    return 12;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(order13contents, rightEllipse))
            {
                if (OrderBoxes[12].Visibility == System.Windows.Visibility.Visible)
                {
                    return 13;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(order14contents, rightEllipse))
            {
                if (OrderBoxes[13].Visibility == System.Windows.Visibility.Visible)
                {
                    return 14;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(order15contents, rightEllipse))
            {
                if (OrderBoxes[14].Visibility == System.Windows.Visibility.Visible)
                {
                    return 15;
                }
                else
                {
                    return 0;
                }
            }
            */
            if (TouchingButton(claimOrder, rightEllipse))
            {
                if (claimOrder.Visibility == System.Windows.Visibility.Visible)
                {
                    return 16;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(markDone, rightEllipse))
            {
                if (markDone.Visibility == System.Windows.Visibility.Visible)
                {
                    return 17;
                }
                else
                {
                    return 0;
                }
            }

            if (TouchingButton(reopenOrder, rightEllipse))
            {
                if (reopenOrder.Visibility == System.Windows.Visibility.Visible)
                {
                    return 18;
                }
                else
                {
                    return 0;
                }
            }

            if (/*TouchingButton(HELPBUTTONONMAINSCREEN, rightEllipse) || */TouchingButton(help, rightEllipse))
            {
                /*
                if (HELPBUTTONONMAINSCREEN.Visibility == System.Windows.Visibility.Visible)
                {
                    return 19;
                }
                else 
                 * */
                if (help.Visibility == System.Windows.Visibility.Visible)
                {
                    return 19;
                }
                else
                {
                    return 0;
                }
            }

            /*
            if (TouchingButton(STARTBUTTONONMAINSCREEN, rightEllipse)
            {
                if (STARTBUTTONONMAINSCREEN.Visibility == System.Windows.Visibility.Visible)
                {
                    return 20;
                }
                else
                {
                    return 0;
                }
            }
             * */

            return 0;
        }

        private void FindPosition(FrameworkElement button, FrameworkElement target)
        {

            if (closing)
            {
                return;
            }

            var buttonTopLeft = button.PointToScreen(new Point());
            var itemTopLeft = target.PointToScreen(new Point());

            top = buttonTopLeft.Y;
            bottom = top + button.ActualHeight;
            left = buttonTopLeft.X;
            right = left + button.ActualWidth;

            //find the middle of item
            itemLeft = itemTopLeft.X + (target.ActualWidth / 2);
            itemTop = itemTopLeft.Y + (target.ActualHeight / 2);
        }

        public bool TouchingButton(FrameworkElement element, FrameworkElement target)
        {
            FindPosition(element, target);

            if (itemTop > top && bottom > itemTop && itemLeft > left && right > itemLeft)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region methods that change your state and call other methods

        private void closeWindow(object sender, EventArgs e)
        {
            ((DispatcherTimer)sender).Stop();
            if (state == 2)
            {
                ClosePopUp();
            }
            if (state == 4)
            {
                CloseClosed();
            }
            if (state == 5)
            {
                CloseHelp();
            }
            playing = true;
        }
        #region methods that touch DispatcherTimer

        private void TransitionToClosed(object sender, EventArgs e)
        {
            ((DispatcherTimer)sender).Stop();
            playing = true;
        }

        private void TransitionToOpened(object sender, EventArgs e)
        {
            ((DispatcherTimer)sender).Stop();
            playing = true;
        }
        #endregion
        #endregion

        #region sample code for setting up Kinect

        #region Window events

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            time.Elapsed += new ElapsedEventHandler(time_Elapsed);
            // Set the Interval to 1 second.
            time.Interval = 1000;
            time.Enabled = false;
            actionTime.Interval = 300;
            actionTime.Enabled = false;
            actionTime.Elapsed += new ElapsedEventHandler(actionTime_Elapsed);
            kinectSensorChooser1.KinectSensorChanged += new DependencyPropertyChangedEventHandler(kinectSensorChooser1_KinectSensorChanged);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            closing = true;
            StopKinect(kinectSensorChooser1.Kinect);
        }

        #endregion

        void kinectSensorChooser1_KinectSensorChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            KinectSensor old = (KinectSensor)e.OldValue;

            StopKinect(old);

            KinectSensor sensor = (KinectSensor)e.NewValue;

            if (sensor == null)
            {
                return;
            }
            var parameters = new TransformSmoothParameters
            {
                Smoothing = 0.05f,
                Correction = 0.9f,
                Prediction = 0.0f,
                JitterRadius = 0.1f,
                MaxDeviationRadius = 0.1f
            };
            sensor.SkeletonStream.Enable(parameters);

            sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(newSensor_AllFramesReady);
            sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
            sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);

            try
            {
                sensor.Start();
            }
            catch (System.IO.IOException)
            {
                kinectSensorChooser1.AppConflictOccurred();
            }
        }

        private void StopKinect(KinectSensor sensor)
        {
            if (sensor != null)
            {
                if (sensor.IsRunning)
                {
                    //stop sensor 
                    sensor.Stop();

                    //stop audio if not null
                    if (sensor.AudioSource != null)
                    {
                        sensor.AudioSource.Stop();
                    }
                }
            }
        }

        #endregion

        #region sample code for dealing with camera input

        Skeleton GetFirstSkeleton(AllFramesReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrameData = e.OpenSkeletonFrame())
            {
                if (skeletonFrameData == null)
                {
                    return null;
                }

                skeletonFrameData.CopySkeletonDataTo(allSkeletons);
                //get the first tracked skeleton
                Skeleton first = (from s in allSkeletons
                                  where s.TrackingState == SkeletonTrackingState.Tracked
                                  select s).FirstOrDefault();

                if (first != null)
                {
                    PointCollection RightPoints = new PointCollection();
                    PointCollection LeftPoints = new PointCollection();
                    PointCollection BodyPoints = new PointCollection();
                    RightPoints.Add(new Point(first.Joints[JointType.HandRight].Position.X, first.Joints[JointType.HandRight].Position.Z));
                    RightPoints.Add(new Point(first.Joints[JointType.HandRight].Position.X, first.Joints[JointType.HandRight].Position.Y));
                    LeftPoints.Add(new Point(first.Joints[JointType.HandLeft].Position.X, first.Joints[JointType.HandLeft].Position.Z));
                    LeftPoints.Add(new Point(first.Joints[JointType.HandLeft].Position.X, first.Joints[JointType.HandLeft].Position.Y));
                    BodyPoints.Add(new Point(first.Joints[JointType.Spine].Position.X, first.Joints[JointType.Spine].Position.Z));
                    BodyPoints.Add(new Point(first.Joints[JointType.Spine].Position.X, first.Joints[JointType.Spine].Position.Y));
                    skeletonHist.Add(skeletonFrameData.Timestamp, BodyPoints);
                    rHist.Add(skeletonFrameData.Timestamp, RightPoints);
                    lHist.Add(skeletonFrameData.Timestamp, LeftPoints);
                    lastIndex += 1;
                }
                return first;
            }
        }

        void GetCameraPoint(Skeleton first, AllFramesReadyEventArgs e)
        {

            using (DepthImageFrame depth = e.OpenDepthImageFrame())
            {
                if (depth == null ||
                    kinectSensorChooser1.Kinect == null)
                {
                    return;
                }

                //Map a joint location to a point on the depth map
                //head
                DepthImagePoint headDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.Head].Position);
                //left hand
                DepthImagePoint leftDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.HandLeft].Position);
                //right hand
                DepthImagePoint rightDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.HandRight].Position);


                //Map a depth point to a point on the color image
                //head
                ColorImagePoint headColorPoint =
                    depth.MapToColorImagePoint(headDepthPoint.X, headDepthPoint.Y,
                    ColorImageFormat.RgbResolution640x480Fps30);
                //left hand
                ColorImagePoint leftColorPoint =
                    depth.MapToColorImagePoint(leftDepthPoint.X, leftDepthPoint.Y,
                    ColorImageFormat.RgbResolution640x480Fps30);
                //right hand
                ColorImagePoint rightColorPoint =
                    depth.MapToColorImagePoint(rightDepthPoint.X, rightDepthPoint.Y,
                    ColorImageFormat.RgbResolution640x480Fps30);

                //Set location
                //CameraPosition(rightEllipse, rightColorPoint);
            }
        }

        private void CameraPosition(FrameworkElement element, ColorImagePoint point)
        {
            //Divide by 2 for width and height so point is right in the middle 
            // instead of in top/left corner
            Canvas.SetLeft(element, point.X - element.Width - 20 / 2);
            Canvas.SetTop(element, point.Y - element.Height - 20 / 2);

        }

        private void CameraHeadPosition(FrameworkElement element, ColorImagePoint point)
        {
            Canvas.SetLeft(element, point.X - element.Width + 10 / 2);
            Canvas.SetTop(element, point.Y - element.Height + 50 / 2);
        }


        private void ScalePosition(FrameworkElement element, Joint joint)
        {
            //convert the value to X/Y
            //Joint scaledJoint = joint.ScaleTo(1280, 720); 

            //convert & scale (.3 = means 1/3 of joint distance)
            //Joint scaledJoint = joint.ScaleTo(640, 720, .3f, .3f);
            Joint scaledJoint = joint.ScaleTo(1350, 700, .3f, .3f);
            float y = scaledJoint.Position.Y * 2;
            Canvas.SetLeft(element, scaledJoint.Position.X);
            Canvas.SetTop(element, y < 670 ? y : 670);
        }

        #endregion

    }
}
