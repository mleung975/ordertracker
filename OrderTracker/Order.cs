﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderTracker
{
    class Order
    {
        int ordernum;
        String orderdetails;
        int hour;
        int minute;
        bool completed;
        String amorpm;
        OrderStatus status = OrderStatus.Normal;

        public Order(int n, String details, int h, int m, String ampm)
        {
            ordernum = n;
            orderdetails = details;
            hour = h;
            minute = m;
            amorpm = ampm;
        }

        public int getOrderNum()
        {
            return ordernum;
        }

        public String getOrderDetails()
        {
            return orderdetails;
        }

        public int getHour()
        {
            return hour;
        }

        public int getMinute()
        {
            return minute;
        }

        public String getAMorPM()
        {
            return amorpm;
        }

        public OrderStatus getStatus()
        {
            return status;
        }

        public void setStatus(OrderStatus newStatus)
        {
            status = newStatus;
        }
    }
}
