How to Build and Run:

From Microsoft Visual Studio, make sure that all of the images in the "Assets" folder are added into the project.  You can do this by clicking on "Show All Files" in the Solution Explorer and then right clicking on every file in the "Assets" folder and selecting "Include in Project".

Also from Microsoft Visual Studio, make sure that the library references for the Kinect SDK and the Coding4Fun library are correctly referenced.

Now just press Build and then Run the application.